## Plugin Via CEP

- npm install @brunoc/ngx-viacep --save

# Plugin Camera

- ionic cordova plugin add cordova-plugin-camera
- npm install @ionic-native/camera

## Plataforma add

- ionic cordova platform add android

## Rodar no celular Fisico

- ionic cordova run android --device

## Rodar no celular emulado

- ionic cordova run android --emulate

## Plugin Mascara

- npm install br-mask --save -E

## Plugin Storage

- ionic cordova plugin add cordova-sqlite-storage
- npm install --save @ionic/storage
