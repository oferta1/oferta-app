import { Usuario } from './../../model/usuario';
import { UsuarioService } from 'src/app/service/usuario.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-oferta',
  templateUrl: './oferta.page.html',
  styleUrls: ['./oferta.page.scss']
})
export class OfertaPage implements OnInit {
  nomeUsuario: Usuario = new Usuario();
  constructor(
    private router: Router,
    private storage: Storage,
    public usuarioService: UsuarioService
  ) {}

  async ngOnInit() {
    await this.storage.get('usuarioLogado').then(u => {
      this.nomeUsuario = u;
    });
    console.log(this.nomeUsuario);
  }

  onClick(oferta: string) {
    this.router.navigate(['menu/lista-oferta', oferta]);
  }

  async logout() {
    await this.storage.remove('usuario');
    await this.storage.remove('usuarioLogado').then(() => {
      this.router.navigate(['/login']);
    });
  }
}
