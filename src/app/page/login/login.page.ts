import { environment } from './../../../environments/environment';
import { Usuario } from './../../model/usuario';
import { MensagemService } from './../../service/mensagem.service';
import { UsuarioService } from './../../service/usuario.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss']
})
export class LoginPage implements OnInit {
  user: Usuario;
  usuarioLogado: Usuario;
  constructor(
    private usuarioService: UsuarioService,
    private mensagemService: MensagemService,
    private router: Router,
    private storage: Storage
  ) {
    this.user = new Usuario();
    this.usuarioLogado = new Usuario();
  }

  ngOnInit() {}

  async logar() {
    const loading = await this.mensagemService.loading({ message: 'Efetuando login...' });
    this.usuarioService.logar(this.user).subscribe(
      async usuario => {
        this.usuarioLogado = usuario as Usuario;
        if (this.usuarioLogado) {
          await this.storage.set('usuarioLogado', this.usuarioLogado);
          this.router.navigate(['/menu/oferta']);
          loading.dismiss();
        } else {
          this.mensagemService.toast({ message: 'Usuario e/ou Senha invalido ou não cadastrado!' });
        }
      },
      error => {
        loading.dismiss();
        this.mensagemService.toast({ message: 'Usuario e/ou Senha invalido ou não cadastrado!' });
      }
    );
  }
}
