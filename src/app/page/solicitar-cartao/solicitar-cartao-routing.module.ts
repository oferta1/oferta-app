import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SolicitarCartaoPage } from './solicitar-cartao.page';

const routes: Routes = [
  {
    path: '',
    component: SolicitarCartaoPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SolicitarCartaoPageRoutingModule {}
