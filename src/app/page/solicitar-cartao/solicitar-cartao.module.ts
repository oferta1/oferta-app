import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SolicitarCartaoPageRoutingModule } from './solicitar-cartao-routing.module';

import { SolicitarCartaoPage } from './solicitar-cartao.page';

import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SolicitarCartaoPageRoutingModule,
    BrMaskerModule
  ],
  declarations: [SolicitarCartaoPage]
})
export class SolicitarCartaoPageModule {}
