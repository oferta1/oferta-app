import { HttpHeaders } from '@angular/common/http';
import { Usuario } from 'src/app/model/usuario';
import { UtilService } from './../../service/util.service';
import { MensagemService } from './../../service/mensagem.service';
import { SolicitarCartao } from './../../model/solicitar-cartao';
import { Component, OnInit } from '@angular/core';
import { NgxViacepService, Endereco, ErroCep } from '@brunoc/ngx-viacep';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { SolicitarCartaoService } from 'src/app/service/solicitar-cartao.service';
import { NgForm } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'app-solicitar-cartao',
  templateUrl: './solicitar-cartao.page.html',
  styleUrls: ['./solicitar-cartao.page.scss']
})
export class SolicitarCartaoPage implements OnInit {
  today = new Date();
  anoAtual: string;
  ano18: string;
  cliente: SolicitarCartao = new SolicitarCartao();
  opcaoFoto: string;
  base64Image: string;
  usuarioLogado: Usuario = new Usuario();
  constructor(
    private viacep: NgxViacepService,
    private camera: Camera,
    private storage: Storage,
    private router: Router,
    private mensagemService: MensagemService,
    public solicitarService: SolicitarCartaoService,
    public utilService: UtilService
  ) {}

  async ngOnInit() {
    this.storage
      .get('usuarioLogado')
      .then(result => {
        this.usuarioLogado = result;
      })
      .catch(e => this.mensagemService.toast({ message: e }));
    this.opcaoFoto = 'F';
    this.base64Image = '';
    this.cliente = new SolicitarCartao();
    this.cliente.documento = '';
    const ano = new Date().getFullYear();
    const calc = ano - 18;
    this.anoAtual = String(ano);
    this.ano18 = String(calc);
  }

  logar(f: NgForm) {
    console.log(f);
  }

  async salvar() {
    // if (this.cliente.documento.length === 0) {
    //   await this.mensagemService.toast({ message: 'Anexar documento!' });
    //   return;
    // }

    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: `Bearer ${this.usuarioLogado.token}`
    });
    const datePipe = new DatePipe('en-US');
    const loading = await this.mensagemService.loading({ message: 'Solicitado...' });
    this.cliente.status = false;
    this.cliente.dataNascimento = null;
    // this.cliente.usuario = this.usuarioLogado;
    this.cliente.rendaMensal = this.cliente.rendaMensal.replace(',', '');
    this.solicitarService.save(this.cliente, headers).subscribe(
      () => {
        loading.dismiss();
        this.mensagemService.toast({ message: 'Solicitação efetuada' });
        this.cliente = new SolicitarCartao();
      },
      error => {
        loading.dismiss();
        this.mensagemService.toast({ message: 'Erro ao Solicitar: ' + error });
      }
    );
  }

  async buscarEndereco() {
    if (this.cliente.cep.length === 8) {
      const loading = await this.mensagemService.loading({ message: 'Aguarde...' });
      this.viacep
        .buscarPorCep(this.cliente.cep)
        .then((endereco: Endereco) => {
          this.cliente.cep = endereco.cep;
          this.cliente.complemento = endereco.complemento;
          this.cliente.logradouro = endereco.logradouro;
          this.cliente.localidade = endereco.localidade;
          this.cliente.bairro = endereco.bairro;
          this.cliente.uf = endereco.uf;
          console.log(endereco);
        })
        .catch((error: ErroCep) => {
          console.log(error.message);
        })
        .finally(() => loading.dismiss());
    }
  }

  async takePicture() {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.DATA_URL,
      encodingType: this.camera.EncodingType.JPEG,
      sourceType:
        this.opcaoFoto === 'F'
          ? this.camera.PictureSourceType.CAMERA
          : this.camera.PictureSourceType.PHOTOLIBRARY,
      mediaType: this.camera.MediaType.PICTURE,
      allowEdit: true
    };
    const loading = await this.mensagemService.loading({ message: 'Aguarde...' });
    this.camera
      .getPicture(options)
      .then(
        imageData => {
          this.base64Image = 'data:image/jpeg;base64,' + imageData;
          this.cliente.documento = this.base64Image;
          console.log('Imagem a ser Salva: ', this.base64Image);
        },
        error => console.error(error)
      )
      .catch(error => console.error(error))
      .finally(() => loading.dismiss());
  }

  async logout() {
    await this.storage.remove('user').then(() => {
      this.router.navigate(['/login']);
    });
  }

  /**
   * token full
   * eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwMDAuMDAwLjAwMC0wMCIsImV4cCI6MjUzNDk5MDQwMH0.tR1P91qFkjp5UMxhC926hQuyXsKFjnlygCQiEW8i7ibSE6SYqoef5SD3LdKnNU7bT-6uklRsF17WjoRPj_6IOA
   */
}
