import { UsuarioService } from 'src/app/service/usuario.service';
import { MensagemService } from './../../service/mensagem.service';
import { CartaoService } from './../../service/cartao.service';
import { Component, OnInit } from '@angular/core';
import { Cartao } from 'src/app/model/cartao';
import { Usuario } from 'src/app/model/usuario';
import { Storage } from '@ionic/storage';
import { Router } from '@angular/router';

class Currency {
  format: string;
  decimal: number;
}

@Component({
  selector: 'app-cartao',
  templateUrl: './cartao.page.html',
  styleUrls: ['./cartao.page.scss']
})
export class CartaoPage implements OnInit {
  usuario: Usuario;
  cartao: Cartao;
  usuarioLogado: Usuario = new Usuario();
  loading: any;
  a: any;
  b: any;
  c: any;
  d: any;

  constructor(
    public cartaoService: CartaoService,
    private storage: Storage,
    private usuarioService: UsuarioService,
    private mensagemService: MensagemService,
    private router: Router
  ) {
    this.usuario = new Usuario();
    this.cartao = new Cartao();
  }

  async ngOnInit() {
    this.usuarioLogado = await this.storage.get('usuarioLogado');
    this.cartaoService
      .cartaoPorUsuario(this.usuarioLogado, this.usuarioLogado.token)
      .subscribe(result => {
        this.cartao = result;
        this.cartaoService.cartaoSelecionado = result;
        if (this.cartao !== null) {
          const numCartao = this.cartao.numeroCartao.split('.');
          this.a = numCartao[0];
          this.b = numCartao[1];
          this.c = numCartao[2];
          this.d = numCartao[3];
        }
      });
  }

  async logout() {
    await this.storage.remove('usuario');
    await this.storage.remove('usuarioLogado').then(() => {
      this.router.navigate(['/login']);
    });
  }

  public money(
    value: number,
    useCurrency = true,
    currency: Currency = null,
    expense: boolean = false
  ) {
    let decimal, format;
    if (currency) {
      decimal = currency['decimal'];
      format = currency['format'];
    } else {
      decimal = 2;
      format = 'R$';
    }
    if (value === undefined || value == null) {
      value = 0;
    } else {
      value = value * 100;
    }
    if (expense) {
      value = Math.abs(value);
    }
    const divider = Math.pow(10, decimal);
    let conversion: string | number = value / divider;
    conversion = conversion.toFixed(decimal).replace('.', ',');
    if (useCurrency) {
      return format + ' ' + conversion;
    }
    return conversion;
  }

  paginaSolicitaCartao() {
    this.router.navigate(['menu/solicitar-cartao']);
  }
}
