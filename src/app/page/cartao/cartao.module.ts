import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { CartaoPageRoutingModule } from './cartao-routing.module';

import { CartaoPage } from './cartao.page';

import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    CartaoPageRoutingModule,
    BrMaskerModule
  ],
  declarations: [CartaoPage]
})
export class CartaoPageModule {}
