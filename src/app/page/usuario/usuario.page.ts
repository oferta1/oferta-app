import { MensagemService } from './../../service/mensagem.service';
import { Usuario } from './../../model/usuario';
import { UtilService } from './../../service/util.service';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { UsuarioService } from 'src/app/service/usuario.service';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { Permissao } from 'src/app/model/permissao';

@Component({
  selector: 'app-usuario',
  templateUrl: './usuario.page.html',
  styleUrls: ['./usuario.page.scss']
})
export class UsuarioPage implements OnInit {
  user: Usuario;
  constructor(
    public utilService: UtilService,
    private usuarioService: UsuarioService,
    private mensagemService: MensagemService,
    private storage: Storage,
    private router: Router
  ) {}

  ngOnInit() {
    this.user = new Usuario();
  }

  onSubmit(f: NgForm) {
    console.log(f);
  }

  async cadastrar(usuario: Usuario) {
    const usuarioLogado = await this.storage.get('usuarioLogado');
    usuario.permissoes.push(this.permissao());
    const loading = await this.mensagemService.loading({ message: 'Cadastrando...' });
    this.usuarioService.salvar(usuario).subscribe(
      () => {
        loading.dismiss();
        this.router.navigate(['/login']);
        this.mensagemService.toast({ message: 'Cadastro efetuado! Faça o Login.' });
      },
      error => {
        loading.dismiss();
        this.router.navigate(['/login']);
        this.mensagemService.toast({
          message: 'Cadastro não efetuado. Tente mais tarde. ' + error
        });
      }
    );
  }

  permissao(): Permissao {
    const permissao = new Permissao();
    permissao.id = 4;
    permissao.nome = 'APP';
    return permissao;
  }
}
