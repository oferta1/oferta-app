import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UsuarioPageRoutingModule } from './usuario-routing.module';

import { UsuarioPage } from './usuario.page';

import { BrMaskerModule } from 'br-mask';

@NgModule({
  imports: [CommonModule, FormsModule, IonicModule, BrMaskerModule, UsuarioPageRoutingModule],
  declarations: [UsuarioPage]
})
export class UsuarioPageModule {}
