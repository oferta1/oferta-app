import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'oferta',
        loadChildren: () => import('../../page/oferta/oferta.module').then(m => m.OfertaPageModule)
      },
      {
        path: 'cartao',
        loadChildren: () => import('../../page/cartao/cartao.module').then(m => m.CartaoPageModule)
      },
      {
        path: 'solicitar-cartao',
        loadChildren: () =>
          import('../../page/solicitar-cartao/solicitar-cartao.module').then(
            m => m.SolicitarCartaoPageModule
          )
      },
      {
        path: 'lista-oferta/:string',
        loadChildren: () =>
          import('../../page/lista-oferta/lista-oferta.module').then(m => m.ListaOfertaPageModule)
      },
      {
        path: '',
        redirectTo: 'oferta',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: 'oferta',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
