import { CartaoService } from './../../service/cartao.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.page.html',
  styleUrls: ['./tabs.page.scss']
})
export class TabsPage implements OnInit {
  constructor(public cartaoSrv: CartaoService) {}

  ngOnInit() {}
}
