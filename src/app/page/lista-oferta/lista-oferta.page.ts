import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { Usuario } from 'src/app/model/usuario';
import { Oferta } from './../../model/oferta';
import { OfertaService } from './../../service/oferta.service';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Storage } from '@ionic/storage';
import { BrMaskDirective, BrMaskModel } from 'br-mask';

class Currency {
  format: string;
  decimal: number;
}

@Component({
  selector: 'app-lista-oferta',
  templateUrl: './lista-oferta.page.html',
  styleUrls: ['./lista-oferta.page.scss']
})
export class ListaOfertaPage implements OnInit {
  ofertasMesDiaSemana: string;
  ofertas: Oferta[];
  usuarioLogado: Usuario = new Usuario();
  constructor(
    private router: Router,
    private ofertaService: OfertaService,
    private storage: Storage
  ) {
    this.ofertasMesDiaSemana = this.descricao(this.router.url.split('/')[3]);
  }

  async ngOnInit() {
    await this.storage.get('usuarioLogado').then(u => {
      this.usuarioLogado = u;
    });
  }

  async ionViewWillEnter() {
    await this.storage.get('usuarioLogado').then(u => {
      this.usuarioLogado = u;
    });
    console.log('entrou');
    this.ofertas = [];
    switch (this.router.url.split('/')[3]) {
      case 'semana':
        this.ofertaService.ofertaSemana(this.usuarioLogado.token).subscribe(result => {
          this.ofertas = result['content'];
        });
        break;
      case 'dia':
        this.ofertaService.ofertaDia(this.usuarioLogado.token).subscribe(result => {
          this.ofertas = result['content'];
        });
        break;
      case 'mes':
        this.ofertaService.ofertaMes(this.usuarioLogado.token).subscribe(result => {
          this.ofertas = result['content'];
        });
        break;
      default:
        break;
    }
    this.ofertas.forEach(modeda => {});
  }

  descricao(texto: string): string {
    switch (texto) {
      case 'semana':
        return 'da Semana';
      case 'dia':
        return 'do Dia';
      case 'mes':
        return 'do Mês';
      default:
        break;
    }
  }

  public money(
    value: number,
    useCurrency = true,
    currency: Currency = null,
    expense: boolean = false
  ) {
    let decimal, format;
    if (currency) {
      decimal = currency['decimal'];
      format = currency['format'];
    } else {
      decimal = 2;
      format = 'R$';
    }
    if (value === undefined || value == null) {
      value = 0;
    } else {
      value = value * 100;
    }
    if (expense) {
      value = Math.abs(value);
    }
    const divider = Math.pow(10, decimal);
    let conversion: string | number = value / divider;
    conversion = conversion.toFixed(decimal).replace('.', ',');
    if (useCurrency) {
      return format + ' ' + conversion;
    }
    return conversion;
  }
}
