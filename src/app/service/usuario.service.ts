import { Observable } from 'rxjs';
import { Usuario } from './../model/usuario';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService extends CrudService<Usuario, number> {
  usuarioLogado: Usuario;
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/usuario');
    this.usuarioLogado = new Usuario();
  }

  salvar(usuario: Usuario): Observable<Usuario> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIwMDAuMDAwLjAwMC0wMCIsImV4cCI6MjUzNDk5MDQwMH0.tR1P91qFkjp5UMxhC926hQuyXsKFjnlygCQiEW8i7ibSE6SYqoef5SD3LdKnNU7bT-6uklRsF17WjoRPj_6IOA'
    });
    usuario.ativo = true;
    return this.save(usuario, headers);
  }

  buscar(cpf: string, token: string): Observable<Usuario> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer ' + token
    });
    let params = new HttpParams();
    params = params.append('cpf', cpf);
    params = params.append('ativo', String(true));
    return this.findAll(headers, params)[0];
  }

  logar(usuario: Usuario): Observable<Usuario> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json'
    });
    return this.http.post<Usuario>(environment.urlApi + '/login', usuario, { headers }); //this.findAll(environment.headers, params);
  }
}
