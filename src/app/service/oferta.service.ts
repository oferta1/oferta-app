import { Observable } from 'rxjs';
import { Oferta } from './../model/oferta';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class OfertaService extends CrudService<Oferta, number> {
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/oferta');
  }

  ofertaSemana(token: string): Observable<Oferta[]> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer ' + token
    });
    let params = new HttpParams();
    params = params.append('periodoOferta', 'PERIODO_SEMANA');

    return this.findAll(headers, params);
  }
  ofertaMes(token: string): Observable<Oferta[]> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer ' + token
    });
    let params = new HttpParams();
    params = params.append('periodoOferta', 'PERIODO_MES');

    return this.findAll(headers, params);
  }
  ofertaDia(token: string): Observable<Oferta[]> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer ' + token
    });
    let params = new HttpParams();
    params = params.append('periodoOferta', 'PERIODO_DIA');

    return this.findAll(headers, params);
  }
}
