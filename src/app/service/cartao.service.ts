import { environment } from './../../environments/environment';
import { Observable } from 'rxjs';
import { Cartao } from './../model/cartao';
import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http';
import { CrudService } from '../dao/crud-service';
import { Usuario } from '../model/usuario';

@Injectable({
  providedIn: 'root'
})
export class CartaoService extends CrudService<Cartao, number> {
  cartaoSelecionado: Cartao = new Cartao();
  constructor(public http: HttpClient) {
    super(http, environment.urlApi + '/cartao');
  }

  cartaoPorUsuario(usuario: Usuario, token: string): Observable<Cartao> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: `Bearer ${token}`
    });
    return this.http.get<Cartao>(environment.urlApi + `/cartao/usuario/${usuario.cpf}`, {
      headers
    }); //this.findAll(headers, params);
  }
}
