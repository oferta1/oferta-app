import { MensagemService } from './mensagem.service';
import { environment } from './../../environments/environment';
import { SolicitarCartao } from './../model/solicitar-cartao';
import { Injectable } from '@angular/core';
import { CrudService } from '../dao/crud-service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SolicitarCartaoService extends CrudService<SolicitarCartao, number> {
  endpoint = environment.urlApi + '/solicitacaoCartao';
  constructor(public http: HttpClient, private mensagemService: MensagemService) {
    super(http, environment.urlApi + '/solicitacaoCartao');
  }

  salvar(cliente: SolicitarCartao, token: string): Observable<SolicitarCartao> {
    const headers = new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer ' + token
    });
    return this.save(cliente, headers);
  }
}
