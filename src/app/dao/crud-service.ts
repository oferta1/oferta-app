import { CrudOperations } from './crud-operations';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
export abstract class CrudService<T, ID> implements CrudOperations<T, ID> {
  constructor(protected http: HttpClient, protected base: string) {}

  save(t: T, headers?: HttpHeaders, params?: HttpParams): Observable<T> {
    return this.http.post<T>(this.base, t, { headers, params });
  }
  update(id: ID, t: T, headers?: HttpHeaders, params?: HttpParams): Observable<T> {
    return this.http.put<T>(this.base + '/' + id, t, { headers, params });
  }
  findOne(id: ID, headers?: HttpHeaders, params?: HttpParams): Observable<T> {
    return this.http.get<T>(this.base + '/' + id, { headers, params });
  }
  findAll(headers?: HttpHeaders, params?: HttpParams): Observable<T[]> {
    return this.http.get<T[]>(this.base, { headers, params });
  }
  delete(id: ID, headers?: HttpHeaders, params?: HttpParams): Observable<any> {
    return this.http.delete<T>(this.base + '/' + id, { headers, params });
  }
}
