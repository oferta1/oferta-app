import { Observable } from 'rxjs';
import { HttpHeaders, HttpParams } from '@angular/common/http';

export interface CrudOperations<T, ID> {
  save(t: T, headers?: HttpHeaders, params?: HttpParams): Observable<T>;
  update(id: ID, t: T, headers?: HttpHeaders, params?: HttpParams): Observable<T>;
  findOne(id: ID, headers?: HttpHeaders, params?: HttpParams): Observable<T>;
  findAll(hedaers?: HttpHeaders, params?: HttpParams): Observable<T[]>;
  delete(id: ID, headers?: HttpHeaders, params?: HttpParams): Observable<any>;
}
