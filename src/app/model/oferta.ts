export class Oferta {
  id: number;
  descricao: string;
  imagem: string;
  nomeProduto: string;
  periodoOferta: string;
  precoAnterior: number;
  precoOferta: number;
}
