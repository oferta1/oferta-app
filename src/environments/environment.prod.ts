import { HttpHeaders } from '@angular/common/http';

export const environment = {
  urlApi: 'http://192.168.0.139:8080/SistemaOferta',
  userName: '',
  headers: new HttpHeaders({
    'Content-type': 'application/json'
  }),
  production: true
};
